package com.trashin.apprecyclefragment.ui.dashboard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.trashin.apprecyclefragment.LoginActivity;
import com.trashin.apprecyclefragment.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private ProgressBar progressBar;
    private EditText credits;
    private EditText username;
    private final int MAX_SCORE = 10000;

    public DashboardFragment(){}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth=FirebaseAuth.getInstance();
        mAuthListener=new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(firebaseAuth.getCurrentUser() == null){
                    getActivity().finishAffinity();
                    startActivity(new Intent(getActivity(),LoginActivity.class));
                }

            }
        };
    }

    @Override
    public void onStart(){
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop(){
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);

        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        final TextView textView = root.findViewById(R.id.text_dashboard);
        dashboardViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });


        progressBar= root.findViewById(R.id.progressBar);
        progressBar.setMax(MAX_SCORE);
        progressBar.setProgress(0);

        credits= root.findViewById(R.id.editTextCredits);
        credits.setEnabled(false);


        username= root.findViewById(R.id.editTextUser);
        username.setText(mAuth.getCurrentUser().getEmail());
        username.setEnabled(false);
        //entra ed esce senza log e questo risulta  nullo.. come risolvere?
        Button btnLogout = root.findViewById(R.id.buttonLogOut);
        btnLogout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mAuth.signOut();

            }
        });
        searchUserInfo(mAuth.getCurrentUser().getUid(),1);
        return root;
    }


    public void searchUserInfo(final String rawValue, final int i) {
        readData(rawValue, new DashboardFragment.FirestoreCallbackRead() {
            @Override
            public void onCallback(Number information[]) {
                if (information[0]== null) {
                    // se è nullo avviso
                    Log.d("msg", "no information about");
                } else {
                   // TextView tv = findWievbyId(R.id.textView);

                    Log.d("msg", "information about rawvalfounded"+"Credits: " + information[0] + "\n");
                    Toast.makeText(getActivity().getApplicationContext(),getString(R.string.your_credits)+information[0],Toast.LENGTH_SHORT).show();
                    progressBar.setProgress(information[0].intValue());
                    credits.setText(information[0].toString());
                }
            }
        });
    }

    private void readData(String userid, final DashboardFragment.FirestoreCallbackRead firestoreCallbackRead) {
        final Number[] information = {null};
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users").whereEqualTo("userid", userid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (!task.getResult().isEmpty()) {
                                information[0] = ((Number) task.getResult().getDocuments().get(0).getData().get("credits"));
                                //Log.d("msg", "getting documents: " +getInformation());
                            } else {
                                Log.d("msg", "no information about rawval");
                            }
                            firestoreCallbackRead.onCallback(information);
                        } else {
                            Log.d("msg", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    /*** DEFINZIONI INTERFACCE DI CALLBACK ***/

    private interface FirestoreCallbackRead {
        void onCallback(Number information[]);
    }

}