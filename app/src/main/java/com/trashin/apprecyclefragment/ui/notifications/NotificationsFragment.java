package com.trashin.apprecyclefragment.ui.notifications;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;

import com.trashin.apprecyclefragment.ConfirmProductFragment;
import com.trashin.apprecyclefragment.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;

public class NotificationsFragment extends Fragment {

    private ListView list;
    private TextView tv;
    private TextView tv2;
    private FirebaseAuth mAuth;
    private NotificationsViewModel notificationsViewModel;
    private Context context;
    ArrayList<DocInfo> docList = null;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        Log.d("msg", "sto creando la view notifiche");
        notificationsViewModel =
                ViewModelProviders.of(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
        mAuth = FirebaseAuth.getInstance();
        list = (ListView) root.findViewById(R.id.listView);
        tv = (TextView)root.findViewById(R.id.editTextNoNoti);
        tv2 = (TextView)root.findViewById(R.id.editTextNoNoti2);
        context = getContext();

        FirebaseDatabase mDb = FirebaseDatabase.getInstance();
        DatabaseReference mRef = mDb.getReference("pending");
        Query q = mRef.getRef();
        q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<DocInfo> array = new ArrayList<>();
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    if (!ds.getValue(DocInfo.class).by.equals(mAuth.getCurrentUser().getUid())){
                        DocInfo doc = ds.getValue(DocInfo.class);
                        doc.setRef(ds.getRef());
                        array.add(doc);
                        Log.d("msg","nome prodotto letto db real ti: "+doc.name);
                    }

                }
                docList = array;
                showList();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }

        });

        return root;
    }

    private void showList(){

        if (docList==null || docList.isEmpty()){ // se condizione gen funziona stamm appost
            tv.setVisibility(View.VISIBLE);
            tv2.setVisibility(View.VISIBLE);
            list.setVisibility(View.INVISIBLE);
            return;
        }
        tv.setVisibility(View.INVISIBLE);
        tv2.setVisibility(View.INVISIBLE);
        list.setVisibility(View.VISIBLE);
        ArrayList<String> productNames = new ArrayList<>();
        for (DocInfo product : docList){
            productNames.add(product.name);
        }
        Log.d("msg","prova del context: "+context);
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(context,R.layout.custom_textview,productNames);// android.R.layout.simple_list_item_1
        list.setAdapter(itemsAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("msg", "item selected number : "+String.valueOf(position));
                // al momento metto un toast che poi sara sostiuito da un fragment
                FragmentManager fm = getActivity().getSupportFragmentManager();
                ConfirmProductFragment f = new ConfirmProductFragment();
                f.setRawValue(docList.get(position).rawValue);
                f.setName(docList.get(position).name);
                f.setBoxRecycleInfo(docList.get(position).box_info);
                f.setProductRecycleInfo(docList.get(position).product_info);
                f.setBy(docList.get(position).by);
                f.setDocReference(docList.get(position).getRef());
                f.show(fm,"fragment_edit_name");
            }
        });


    }
    @Override
    public void onResume() {
        Log.d("msg","resumando");
        super.onResume();
    }
}
@IgnoreExtraProperties
class DocInfo implements Serializable{

    public String box_info;
    public String by;
    public String name;
    public String product_info;
    public String rawValue;

    public DatabaseReference getRef() {
        return ref;
    }

    public void setRef(DatabaseReference ref) {
        this.ref = ref;
    }

    private DatabaseReference ref;
    public DocInfo(){}

}