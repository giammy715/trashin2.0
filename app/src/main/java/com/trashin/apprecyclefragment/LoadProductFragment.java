package com.trashin.apprecyclefragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class LoadProductFragment extends DialogFragment {


    private FirebaseAuth mAuth;
    private EditText text;
    private EditText text1;
    private EditText text2;
    private String rawValue;
    private Button btnClose;
    private Button btnUpload;


    public String getRawValue() {
        return rawValue;
    }

    public void setRawValue(String rawValue) {
        this.rawValue = rawValue;
        Log.d("msg","received raw val: "+this.rawValue);
    }

    public LoadProductFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        final View view = inflater.inflate(R.layout.load_product_fragment,container);

        btnClose = (Button)view.findViewById(R.id.button);
        btnUpload = (Button)view.findViewById(R.id.button2);

        btnClose.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        btnUpload.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                text = (EditText)view.findViewById(R.id.editText);
                text1 = (EditText)view.findViewById(R.id.editText2);
                text2 = (EditText)view.findViewById(R.id.editText3);
                String informations[] = {text.getText().toString(), text1.getText().toString(), text2.getText().toString()};
                Log.d("msg",informations.toString());
                if(checkInput(informations)) {
                    loadProductInfo(rawValue, informations);
                    dismiss();
                }
                else Toast.makeText(getActivity().getApplicationContext(),R.string.no_valid,Toast.LENGTH_SHORT).show();
            }
        });
        mAuth=FirebaseAuth.getInstance();
        return view;
    }

    public void loadProductInfo(String rawValue, String info[]) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference();
        final Context context = getActivity().getApplicationContext();
        Map<String, Object> product = new HashMap<>();
        product.put("rawValue", rawValue);
        product.put("name", info[0]);
        product.put("box_info",info[1]);
        product.put("product_info",info[2]);
        product.put("by",mAuth.getCurrentUser().getUid());
        myRef.child("pending").child(rawValue).setValue(product)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("msg","message added succes");
                        Toast.makeText(context,R.string.success,Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("msg","message added failed");
                        Toast.makeText(context,R.string.problem,Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private boolean checkInput(String informations[]){
        boolean valid = false;
        if( (!informations[0].isEmpty()) && (!informations[1].isEmpty()) && (!informations[2].isEmpty())){ // possibile anche aggiungere altre condizioni che pero ora non so
            valid = true;
        }
        Log.d("msg", "end check input with validity: "+valid);
        return valid;
    }


}
