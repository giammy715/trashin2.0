package com.trashin.apprecyclefragment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int RC_SIGN_IN = 9002;
    private GoogleSignInClient mGoogleSignInClient;
    private EditText email;
    private EditText pass;
    private FirebaseAuth mAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        setContentView(R.layout.activity_login);

        findViewById(R.id.sign_in_button).setOnClickListener(this);
        findViewById(R.id.btn_1).setOnClickListener(this);
        findViewById(R.id.reg_button).setOnClickListener(this);


        mAuth = FirebaseAuth.getInstance();
        email = findViewById(R.id.email_text);
        pass = findViewById(R.id.pass_text);

        new InternetCheck(new InternetCheck.Consumer() {
            @Override
            public void accept(Boolean internet) {
                if (internet == false){
                    Toast.makeText(getApplicationContext(),R.string.no_internet,Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (mAuth.getCurrentUser()==null){
            // No user, send to login.
            Log.d("LOGIN","user nullo");
        } else {
            // There is a user, check for email verification.
            if (mAuth.getCurrentUser().isEmailVerified()) {
                finishAffinity();
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                Log.i("State_chancge", "stato cambiato");
            } else {
                mAuth.signOut();
                Log.d("LOGIN","singout perche email non ver");
            }
        }


       /*
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
               // if (firebaseAuth.getCurrentUser() != null && firebaseAuth.getCurrentUser().isEmailVerified()) {

                if (firebaseAuth.getCurrentUser() != null) {
                    finishAffinity();
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    Log.i("State_chancge", "stato cambiato");
                }


                if (firebaseAuth.getCurrentUser()==null){
                    // No user, send to login.
                    Log.d("LOGIN","user nullo");
                } else {
                    // There is a user, check for email verification.
                    if (firebaseAuth.getCurrentUser().isEmailVerified()) {
                        finishAffinity();
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        Log.i("State_chancge", "stato cambiato");
                    } else {

                        firebaseAuth.signOut();
                        Log.d("LOGIN","singout perche email non ver");
                    }
                }
            }
        };
        */


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void createAccountNormal() {
        if (email.getText().toString().isEmpty() || pass.getText().toString().isEmpty() || pass.getText().toString().length()<6) {
            if(email.getText().toString().isEmpty() || pass.getText().toString().isEmpty() )
                Toast.makeText(getApplicationContext(), R.string.error_log, Toast.LENGTH_SHORT).show();
            else Toast.makeText(getApplicationContext(), R.string.pwd_short, Toast.LENGTH_SHORT).show();

        } else {
            mAuth.createUserWithEmailAndPassword(email.getText().toString(), pass.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.i("Mount:reg", "createUserWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                user.sendEmailVerification()
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Toast.makeText(LoginActivity.this,R.string.registraion_success,
                                                            Toast.LENGTH_SHORT).show();
                                                    Log.d("Firebase Mail:", "Email sent.");
                                                }
                                            }
                                        });

                                boolean isNew = task.getResult().getAdditionalUserInfo().isNewUser();
                                Log.i("FireBaseAuth", "is new user" + isNew);
                                if (isNew) {
                                    loadUserInfo(user.getUid());
                                }

                            } else {
                                // If sign in fails, display a message to the user.
                                Log.i("Trashin registration", "createUserWithEmail:failure", task.getException());
                                Toast.makeText(LoginActivity.this,R.string.registraion_failed,
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }


    private void singInNormal() {
        if (email.getText().toString().isEmpty() || pass.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), R.string.error_log, Toast.LENGTH_SHORT).show();

        } else {

            mAuth.signInWithEmailAndPassword(email.getText().toString(), pass.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI
                                if(mAuth.getCurrentUser()!= null && mAuth.getCurrentUser().isEmailVerified()){
                                    finishAffinity();
                                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                }
                                else{
                                    Toast.makeText(LoginActivity.this,R.string.verify_mail,
                                            Toast.LENGTH_SHORT).show();
                                }

                                Log.d("Mount:sign", "signInWithEmail:success");

                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w("Mount:sing", "signInWithEmail:failure", task.getException());
                                Toast.makeText(LoginActivity.this,R.string.do_registraion,
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }


    private void signInWithGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    //questa funzione viene chiamata al termine dell activityforresult di sopra restituendo quei par
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
                Log.i("mountain_signin", "signInResult:success" + account.getEmail());
                Toast.makeText(LoginActivity.this, R.string.gogle_auth_succes, Toast.LENGTH_SHORT).show();
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                e.printStackTrace();
                Log.w("mountain_signin", "signInResult:failed code=" + e.getStatusCode());
                Toast.makeText(LoginActivity.this,R.string.gogle_auth_failed,
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    //SING IN A FIRE BASE CON GOOGLE
    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        Log.i("FireBaseAuth", "firebaseAuthWithGoogle:" + account.getId() + account.getIdToken());

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.i("FireBaseAuth", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            boolean isNew = task.getResult().getAdditionalUserInfo().isNewUser();
                            Log.i("FireBaseAuth", "is new user" + isNew);
                            if (isNew) {
                                loadUserInfo(user.getUid());
                            }

                            if(user!= null && user.isEmailVerified()){
                                finishAffinity();
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            }

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.i("FireBaseAuth", "signInWithCredential:failure", task.getException());
                        }
                    }
                });
    }


    public void loadUserInfo(final String userid) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> user = new HashMap<>();

        user.put("userid", userid);
        user.put("credits", 0);


        // aggiungo ora l'elemento alla collezione di documenti, dove ogni documento è un "obj" json
        db.collection("users").document(userid).set(user, SetOptions.merge()).
                addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("msg", "User added with ID: " + userid);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("msg", "Error adding user", e);
                    }
                });

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        //finishAffinity();

    }

    //Funzioni on clic dei bottoni.
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signInWithGoogle();
                break;
            case R.id.btn_1:
                singInNormal();
                break;
            case R.id.reg_button:
                createAccountNormal();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
        return;

    }

}


class InternetCheck extends AsyncTask<Void,Void,Boolean> {

    private Consumer mConsumer;
    public  interface Consumer { void accept(Boolean internet); }

    public  InternetCheck(Consumer consumer) { mConsumer = consumer; execute(); }

    @Override protected Boolean doInBackground(Void... voids) { try {
        Socket sock = new Socket();
        sock.connect(new InetSocketAddress("8.8.8.8", 53), 1500);
        sock.close();
        return true;
    } catch (IOException e) { return false; } }

    @Override protected void onPostExecute(Boolean internet) { mConsumer.accept(internet); }
}
