package com.trashin.apprecyclefragment;

import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class ConfirmProductFragment extends DialogFragment {


    public String getRawValue() {
        return rawValue;
    }

    public void setRawValue(String rawValue) {
        this.rawValue = rawValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductRecycleInfo() {
        return productRecycleInfo;
    }

    public void setProductRecycleInfo(String productRecycleInfo) {
        this.productRecycleInfo = productRecycleInfo;
    }

    public String getBoxRecycleInfo() {
        return boxRecycleInfo;
    }

    public void setBoxRecycleInfo(String boxRecycleInfo) {
        this.boxRecycleInfo = boxRecycleInfo;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    private String productRecycleInfo;
    private String boxRecycleInfo;
    private String by;
    private String rawValue;
    private String name;
    private DatabaseReference docReference;
    private EditText productName;
    private EditText boxInfo;
    private EditText prodInfo;
    private Button confirmButton;
    private Button denyButton;

    public ConfirmProductFragment() {
    }

    public DatabaseReference getDocReference() {
        return docReference;
    }

    public void setDocReference(DatabaseReference docReference) {
        this.docReference = docReference;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.confirm_product_fragment, container);

        productName = view.findViewById(R.id.editTextPNN);
        productName.setText(name);
        productName.setMovementMethod(new ScrollingMovementMethod());

        prodInfo = view.findViewById(R.id.editTextPRI);
        prodInfo.setText(productRecycleInfo);
        prodInfo.setMovementMethod(new ScrollingMovementMethod());

        boxInfo = view.findViewById(R.id.editTextBRI);
        boxInfo.setText(boxRecycleInfo);
        boxInfo.setMovementMethod(new ScrollingMovementMethod());


        confirmButton = view.findViewById(R.id.button6);
        denyButton = view.findViewById(R.id.button5);




        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                docReference
                        .removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d("msg", "DocumentSnapshot successfully deleted!");

                                FirebaseFirestore db = FirebaseFirestore.getInstance();
                                Map<String, Object> product = new HashMap<>();
                                product.put("rawValue", rawValue);
                                product.put("name", name);
                                product.put("box_info", boxRecycleInfo);
                                product.put("product_info", productRecycleInfo);

                                final Context context = getActivity().getApplicationContext();

                                db.collection("products").add(product).
                                        addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                            @Override
                                            public void onSuccess(DocumentReference documentReference) {
                                                Log.d("msg", "DocumentSnapshot added with ID: " + documentReference.getId());
                                                Toast.makeText(context, R.string.thxconfirm, Toast.LENGTH_SHORT).show();
                                                dismiss();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.d("msg", "Error adding document", e);
                                                Toast.makeText(context, R.string.problem, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                //aggiorno anche i crediti utente che ha creato l'elemento
                                DocumentReference userRef = db.collection("users").document(by);
                                userRef.update("credits", FieldValue.increment(1));
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w("msg", "Error deleting document", e);
                            }
                        });
            }
        });

        denyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context context = getActivity().getApplicationContext();
                docReference
                        .removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d("msg", "DocumentSnapshot successfully deleted!");
                                Toast.makeText(context, R.string.thxdeny, Toast.LENGTH_SHORT).show();
                                dismiss();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w("msg", "Error deleting document", e);
                            }
                        });
            }
        });
        return view;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("msg","prova su onDestryvirew");
    }

}
